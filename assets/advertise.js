var advertise = {
	getAdvertisers : function(company, count) {
		if(count == undefined) {
			count = 2
		}
		
		$.ajax({
			type: 'GET',
			url: 'http://localhost:3000/api/advertisement/' + company + '/' + count + '.json',
			dataType: 'json',
			success: function(data) {
				$.each(data, function(key, val) {
					if(val.url) {
						$('ul.advertisements').append('<li><a href="' + val.url + '" target="_blank"><img src="' + val.image + '" alt="' + val.name + '" /></a></li>');
					}
					else {
						$('ul.advertisements').append('<li><img src="' + val.image + '" alt="' + val.name + '" /></li>');
					}
				});
			}
		});
	}
};

$(function() {
	advertise.getAdvertisers('CC');
});