var domain = 'http://localhost:3000',
		container = 'div.feed-inner-container';

var social = {
	clearLoader : function(element, callback) {
		$(element).find('div.loading').remove();
		if(typeof callback == 'function') callback();
	},
	
	facebook : {
		lastPost : function() {}
	},
	
	twitter : {
		userTimeline : function() {
			var thisContainer = $(container + '.twitter');
			
			$.ajax({
				type: 'GET',
				url: domain + '/api/social/colour-couture/twitter/user_timeline/1.json',
				dataType: 'json',
				success: function(item) {
					thisContainer.append('<div class="thumb"><img src="' + item[0].user.profile_image_url + '" alt="' + item[0].user.name + '" /></div>');
					thisContainer.append('<div class="followUs">Follow Us<br /><a href="http://www.twitter.com/' + item[0].user.screen_name + '" target="_blank">@' + item[0].user.screen_name + '</a></div>');
					
					var tweet = item[0].text;
					tweet = tweet.replace(/http:\/\/\S+/g,  '<a href="$&" target="_blank">$&</a>');
				  tweet = tweet.replace(/\s(@)(\w+)/g,    ' @<a href="http://twitter.com/$2" target="_blank">$2</a>');
					thisContainer.append('<div class="tweet">' + tweet + '</div>');
				},
				error: function() {
					thisContainer.append('<div class="errorMsg">An error occured when loading this feed.</div>');
				},
				complete: function() {
					social.clearLoader(thisContainer);
				}
			});
		}
	},
	
	instagram : {
		latestUploads : function() {
			var thisContainer = $(container + '.instagram');
			
			$.ajax({
				type: 'GET',
				url: domain + '/api/social/colour-couture/instagram/5.json',
				dataType: 'json',
				success: function(item) {
					social.clearLoader(thisContainer, function() {
						$(thisContainer).append('<div class="followUs">Follow Us<br /><a href="http://instagram.com/colourcouturetanning" target="_blank">colourcouturetanning</a></div>');
						
						//Loop through data to grab uploaded photos
						$.each(item.data, function(index, val) {
							thisContainer.find('ul.uploads').append('<li><a href="' + item.data[index].link + '" target="_blank"><img src="' + item.data[index].images.thumbnail.url + '" alt="" /></a></li>');
						});
					});
				},
				error: function() {
					thisContainer.append('<div class="errorMsg">An error occured when loading this feed.</div>');
				},
				complete: function() {
					social.clearLoader(thisContainer);
				}
			});
		}
	}
};

/*
 * Newsletter SignUp
 */
var newsletter = {
	init: function() {
		$('button#newsletterSignUpBtn').click(function() {
			var email = $('input#newsletterEmail').val();
			
			if(email != "") {
				newsletter.signup(email, 'Thank you for signing up!');	
			} else {
				newsletter.invalid('Please enter a valid email!');
			}
			
			return false;
		});	
	},
	
	signup : function(email, msg) {
		var signup = $('div.signup');
		
		$.ajax({
			type: 'POST',
			url: domain + '/api/newsletter/' + encodeURIComponent(email) + '/CC/',
			crossDomain: true,
			success: function() {},
			complete: function() {
				signup.find('fieldset').fadeOut(function() {
					signup.find('div.msg').html(msg).fadeIn();	
				});
			}
		});
	},
	
	invalid : function(msg, callback) {
		var signup = $('div.signup');
		
		signup.find('fieldset').fadeToggle(function() {
			signup.find('div.msg').html(msg).fadeIn(function() {
				
				signup.find('div.msg').delay(2000).fadeOut(function() {
					signup.find('fieldset').fadeIn();
				});
				
			});
		});
		
		if(typeof callback == 'function') callback();
	}
};

$(function() {
	newsletter.init();
	social.facebook.lastPost();
	social.twitter.userTimeline();
	social.instagram.latestUploads();
});